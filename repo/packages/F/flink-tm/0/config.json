{
    "$schema": "http://json-schema.org/schema#",
    "properties": {
        "env": {
            "additionalProperties": false,
            "type": "object",
            "properties": {
                "java_home": {
                    "description": "The path to the Java installation to use (DEFAULT: system's default Java installation, if found). Needs to be specified if the startup scripts fail to automatically resolve the java home directory. Can be specified to point to a specific java installation or version. If this option is not specified, the startup scripts also evaluate the $JAVA_HOME environment variable.",
                    "type": "string",
                    "default": ""
                },
                "java_opts": {
                    "description":"Set custom JVM options. This value is respected by Flink's start scripts, both JobManager and TaskManager, and Flink's YARN client. This can be used to set different garbage collectors or to include remote debuggers into the JVMs running Flink's services. Use env.java.opts.jobmanager and env.java.opts.taskmanager for JobManager or TaskManager-specific options, respectively",
                    "type":"string",
                    "default":"-XX:+UseG1GC -Dfile.encoding=UTF-8"
                },
                "log_dir": {
                    "description":"Defines the directory where the Flink logs are saved. It has to be an absolute path. Defaults to the log directory under Flink's home",
                    "type":"string",
                    "default":""
                }
            }
        },
        "taskmanager":{
            "additionalProperties": false,
            "description": "Flink TaskManager configuration properties",
            "properties": {
                "cpus": {
                    "description": "CPU shares to allocate to each Flink TaskManager.",
                    "type": "number",
                    "default": 1,
                    "minimum": 1
                },
                "mem": {
                    "description": "Memory to allocate to each Flink TaskManager.",
                    "type": "number",
                    "default": 4096.0,
                    "minimum": 4096.0
                },
                "disk": {
                    "description": "Disk (MB) to allocate to each Jobmanager instance.",
                    "type": "integer",
                    "minimum": 2000,
                    "default": 8000
                },
                "instances": {
                    "description": "Number of instances we should have running by mesos.",
                    "type": "integer",
                    "default": 6,
                    "minimum": 1
                },
                "force-pull-image":{
                    "description":"Forces docker to re-pull the image.",
                    "type":"boolean",
                    "default":true
                },
                "java_opts": {
                    "description": "TaskManager-specific JVM options",
                    "type": "string",
                    "default": ""
                },
                "rpc": {
                    "additionalProperties": false,
                    "type": "object",
                    "properties": {
                        "address": {
                            "description": "The hostname of the network interface that the TaskManager binds to. By default, the TaskManager searches for network interfaces that can connect to the JobManager and other TaskManagers. This option can be used to define a hostname if that strategy fails for some reason. Because different TaskManagers need different values for this option, it usually is specified in an additional non-shared TaskManager-specific config file",
                            "type": "string",
                            "default": ""
                        },
                        "port": {
                            "description": "The task manager's IPC port",
                            "type": "integer",
                            "default": 0
                        }
                    }
                },
                "data_port": {
                    "description": "The task manager's port used for data exchange operations",
                    "type": "integer",
                    "default": 0
                },
                "memory": {
                    "additionalProperties": false,
                    "type": "object",
                    "properties": {
                        "size": {
                            "description": "The amount of memory (in megabytes) that the task manager reserves on-heap or off-heap (depending on taskmanager.memory.off-heap) for sorting, hash tables, and caching of intermediate results. If unspecified (-1), the memory manager will take a fixed ratio with respect to the size of the task manager JVM as specified by taskmanager.memory.fraction",
                            "type": "integer",
                            "default": -1
                        },
                        "fraction": {
                            "description": "The relative amount of memory (with respect to taskmanager.heap.mb) that the task manager reserves for sorting, hash tables, and caching of intermediate results. For example, a value of 0.8 means that a task manager reserves 80% of its memory (on-heap or off-heap depending on taskmanager.memory.off-heap) for internal data buffers, leaving 20% of free memory for the task manager's heap for objects created by user-defined functions. This parameter is only evaluated, if taskmanager.memory.size is not set",
                            "type": "number",
                            "default": 0.7
                        },
                        "off_heap": {
                            "description":"If set to true, the task manager allocates memory which is used for sorting, hash tables, and caching of intermediate results outside of the JVM heap. For setups with larger quantities of memory, this can improve the efficiency of the operations performed on the memory",
                            "type":"boolean",
                            "default":false
                        },
                        "segment_size": {
                            "description":"The size of memory buffers used by the memory manager and the network stack in bytes",
                            "type":"integer",
                            "default":32768
                        },
                        "preallocate": {
                            "description":"Can be either of true or false. Specifies whether task managers should allocate all managed memory when starting up",
                            "type":"boolean",
                            "default":false
                        }
                    }
                },
                "debug_memory_startLogThread": {
                    "description":"Causes the TaskManagers to periodically log memory and Garbage collection statistics. The statistics include current heap-, off-heap, and other memory pool utilization, as well as the time spent on garbage collection, by heap memory pool",
                    "type":"boolean",
                    "default":false
                },
                "debug_memory_logIntervalMs": {
                    "description":"The interval (in milliseconds) in which the TaskManagers log the memory and garbage collection statistics. Only has an effect, if taskmanager.debug.memory.startLogThread is set to true",
                    "type":"integer",
                    "default":5000
                },
                "tmp_dirs": {
                    "description":"The interval (in milliseconds) in which the TaskManagers log the memory and garbage collection statistics. Only has an effect, if taskmanager.debug.memory.startLogThread is set to true",
                    "type":"string",
                    "default":""
                },
                "network": {
                    "additionalProperties": false,
                    "type": "object",
                    "properties": {
                        "numberOfBuffers": {
                            "description":"The number of buffers available to the network stack. This number determines how many streaming data exchange channels a TaskManager can have at the same time and how well buffered the channels are. If a job is rejected or you get a warning that the system has not enough buffers available, increase this value",
                            "type":"integer",
                            "default":2048
                        },
                        "defaultIOMode": {
                            "description":"The implementation to use for spillable/spilled intermediate results, which have both synchronous and asynchronous implementations: sync or async",
                            "type":"string",
                            "default":"sync"
                        }
                    }
                },
                "runtime": {
                    "additionalProperties": false,
                    "type": "object",
                    "properties": {
                        "hashjoin_bloom_filters": {
                            "description": "Flag to activate/deactivate bloom filters in the hybrid hash join implementation. In cases where the hash join needs to spill to disk (datasets larger than the reserved fraction of memory), these bloom filters can greatly reduce the number of spilled records, at the cost some CPU cycles",
                            "type": "boolean",
                            "default": false
                        },
                        "max_fan": {
                            "description": "The maximal fan-in for external merge joins and fan-out for spilling hash tables. Limits the number of file handles per operator, but may cause intermediate merging/partitioning, if set too small",
                            "type": "integer",
                            "default": 128
                        },
                        "sort_spilling_threshold": {
                            "description": "A sort operation starts spilling when this fraction of its memory budget is full",
                            "type": "number",
                            "default": 0.8
                        }
                    }
                }
            },
            "required": [
                "cpus",
                "mem",
                "instances"
            ],
            "type": "object"
        },
        "blob": {
            "additionalProperties": false,
            "type": "object",
            "properties": {
                "server_port": {
                    "description": "Port definition for the blob server (serving user jar's) on the Taskmanagers",
                    "type": "integer",
                    "default": 0
                },
                "storage_dirs": {
                    "description":"Directory for storing blobs (such as user jar's) on the TaskManagers",
                    "type":"string",
                    "default":""
                },
                "fetch_retries": {
                    "description": "The number of retries for the TaskManager to download BLOBs (such as JAR files) from the JobManager",
                    "type": "integer",
                    "default": 50
                },
                "fetch_num_concurrent": {
                    "description": "The number concurrent BLOB fetches (such as JAR file downloads) that the JobManager serves",
                    "type": "integer",
                    "default": 50
                },
                "fetch_backlog": {
                    "description": "The maximum number of queued BLOB fetches (such as JAR file downloads) that the JobManager allows",
                    "type": "integer",
                    "default": 1000
                }
            }
        },
        "fs": {
            "additionalProperties": false,
            "type": "object",
            "properties": {
                "default_scheme": {
                    "description": "The default filesystem scheme to be used, with the necessary authority to contact, e.g. the host:port of the NameNode in the case of HDFS (if needed). By default, this is set to file:/// which points to the local filesystem. This means that the local filesystem is going to be used to search for user-specified files without an explicit scheme definition. As another example, if this is set to hdfs://localhost:9000/, then a user-specified file path without explicit scheme definition, such as /user/USERNAME/in.txt, is going to be transformed into hdfs://localhost:9000/user/USERNAME/in.txt. This scheme is used ONLY if no other scheme is specified (explicitly) in the user-provided URI",
                    "type": "string",
                    "default": ""
                },
                "overwrite_files": {
                    "description": "The maximum number of queued BLOB fetches (such as JAR file downloads) that the JobManager allows",
                    "type": "boolean",
                    "default": false
                },
                "output_always_create_directory": {
                    "description": "The maximum number of queued BLOB fetches (such as JAR file downloads) that the JobManager allows",
                    "type": "boolean",
                    "default": false
                },
                "hadoopconf": {
                    "description":"The absolute path to the Hadoop File System's (HDFS) configuration directory (OPTIONAL VALUE). Specifying this value allows programs to reference HDFS files using short URIs (hdfs:///path/to/files, without including the address and port of the NameNode in the file URI). Without this option, HDFS files can be accessed, but require fully qualified URIs like hdfs://address:port/path/to/files. This option also causes file writers to pick up the HDFS's default values for block sizes and replication factors. Flink will look for the \"core-site.xml\" and \"hdfs-site.xml\" files in the specified directory",
                    "type":"string",
                    "default":"http://master.mesos/service/hdfs/v1/endpoints"
                },
                "hdfs_default": {
                    "description": "The absolute path of Hadoop's own configuration file hdfs-default.xml",
                    "type":"string",
                    "default":"" 
                },
                "hdfs_site": {
                    "description": "The absolute path of Hadoop's own configuration file hdfs-site.xml",
                    "type":"string",
                    "default":"" 
                }
            }
        },
        "compiler": {
            "additionalProperties": false,
            "type": "object",
            "properties": {
                "delimited_informat_max_line_samples": {
                    "description": "The maximum number of line samples taken by the compiler for delimited inputs. The samples are used to estimate the number of records. This value can be overridden for a specific input with the input format's parameters",
                    "type": "integer",
                    "default": 10
                },
                "delimited_informat_min_line_samples": {
                    "description": "The minimum number of line samples taken by the compiler for delimited inputs. The samples are used to estimate the number of records. This value can be overridden for a specific input with the input format's parameters",
                    "type": "integer",
                    "default": 2
                },
                "delimited_informat_max_sample_len": {
                    "description": "The maximal length of a line sample that the compiler takes for delimited inputs. If the length of a single sample exceeds this value (possible because of misconfiguration of the parser), the sampling aborts. This value can be overridden for a specific input with the input format's parameters",
                    "type": "integer",
                    "default": 2097152
                }
            }
        },
        "akka": {
            "additionalProperties": false,
            "type": "object",
            "properties": {
                "ask_timeout": {
                    "description": "Timeout used for all futures and blocking Akka calls. If Flink fails due to timeouts then you should try to increase this value. Timeouts can be caused by slow machines or a congested network. The timeout value requires a time-unit specifier (ms/s/min/h/d)",
                    "type": "string",
                    "default": "10 s"
                },
                "lookup_timeout": {
                    "description": "Timeout used for the lookup of the JobManager. The timeout value has to contain a time-unit specifier (ms/s/min/h/d)",
                    "type": "string",
                    "default": "10 s"
                },
                "frame_size": {
                    "description": "Maximum size of messages which are sent between the JobManager and the TaskManagers. If Flink fails because messages exceed this limit, then you should increase it. The message size requires a size-unit specifier",
                    "type": "string",
                    "default": "10485760b"
                },
                "watch": {
                    "additionalProperties": false,
                    "type": "object",
                    "properties": {
                        "heartbeat_interval": {
                            "description": "Heartbeat interval for Akka's DeathWatch mechanism to detect dead TaskManagers. If TaskManagers are wrongly marked dead because of lost or delayed heartbeat messages, then you should increase this value",
                            "type": "string",
                            "default": "10 s"
                        },
                        "heartbeat_pause": {
                            "description": "Acceptable heartbeat pause for Akka's DeathWatch mechanism. A low value does not allow a irregular heartbeat",
                            "type": "string",
                            "default": "60 s"
                        },
                        "threshold": {
                            "description": "Threshold for the DeathWatch failure detector. A low value is prone to false positives whereas a high value increases the time to detect a dead TaskManager",
                            "type": "integer",
                            "default": 12
                        }
                    }
                },
                "transport": {
                    "additionalProperties": false,
                    "type": "object",
                    "properties": {
                        "heartbeat_interval": {
                            "description": "Heartbeat interval for Akka's transport failure detector. Since Flink uses TCP, the detector is not necessary. Therefore, the detector is disabled by setting the interval to a very high value. In case you should need the transport failure detector, set the interval to some reasonable value. The interval value requires a time-unit specifier (ms/s/min/h/d)",
                            "type": "string",
                            "default": "1000 s"
                        },
                        "heartbeat_pause": {
                            "description": "Acceptable heartbeat pause for Akka's transport failure detector. Since Flink uses TCP, the detector is not necessary. Therefore, the detector is disabled by setting the pause to a very high value. In case you should need the transport failure detector, set the pause to some reasonable value. The pause value requires a time-unit specifier (ms/s/min/h/d)",
                            "type": "string",
                            "default": "600 s"
                        },
                        "threshold": {
                            "description": "Threshold for the transport failure detector. Since Flink uses TCP, the detector is not necessary and, thus, the threshold is set to a high value ",
                            "type": "integer",
                            "default": 300
                        }
                    }
                },
                "tcp_port": {
                    "description": "",
                    "type": "integer",
                    "default": 0
                },
                "tcp_timeout": {
                    "description": "Timeout for all outbound connections. If you should experience problems with connecting to a TaskManager due to a slow network, you should increase this value",
                    "type": "string",
                    "default": "10 s"
                },
                "throughput": {
                    "description": "Number of messages that are processed in a batch before returning the thread to the pool. Low values denote a fair scheduling whereas high values can increase the performance at the cost of unfairness ",
                    "type": "integer",
                    "default": 15
                },
                "log_lifecycle_events": {
                    "description": "Turns on the Akka's remote logging of events. Set this value to 'true' in case of debugging",
                    "type": "boolean",
                    "default": false
                },
                "startup_timeout": {
                    "description": "Timeout after which the startup of a remote component is considered being failed ",
                    "type": "string",
                    "default": "10 s"
                }
            }
        },
        "recovery": {
            "additionalProperties": false,
            "type": "object",
            "properties": {
                "mode": {
                    "description": "Defines the recovery mode used for the cluster execution. Currently, Flink supports the 'standalone' mode where only a single JobManager runs and no JobManager state is checkpointed. The high availability mode 'zookeeper' supports the execution of multiple JobManagers and JobManager state checkpointing. Among the group of JobManagers, ZooKeeper elects one of them as the leader which is responsible for the cluster execution. In case of a JobManager failure, a standby JobManager will be elected as the new leader and is given the last checkpointed JobManager state. In order to use the 'zookeeper' mode, it is mandatory to also define the recovery.zookeeper.quorum configuration value",
                    "type": "string",
                    "default": "zookeeper"
                },
                "zookeeper": {
                    "additionalProperties": false,
                    "type": "object",
                    "properties": {
                        "quorum": {
                            "description": "Defines the ZooKeeper quorum URL which is used to connet to the ZooKeeper cluster when the 'zookeeper' recovery mode is selected",
                            "type": "string",
                            "default": "master.mesos:2181"
                        },
                        "path": {
                            "additionalProperties": false,
                            "type": "object",
                            "properties": {
                                "root": {
                                    "description": "Defines the root dir under which the ZooKeeper recovery mode will create znodes",
                                    "type": "string",
                                    "default": "/flink"
                                },
                                "namespace": {
                                    "description": "Defines the subdirectory under the root dir where the ZooKeeper HA mode will create znodes. This allows to isolate multiple applications on the same ZooKeeper",
                                    "type": "string",
                                    "default": "/default_ns"
                                },
                                "jobgraphs": {
                                    "description": "",
                                    "type": "string",
                                    "default": "/jobgraphs"
                                },
                                "checkpoints": {
                                    "description": "",
                                    "type": "string",
                                    "default": "/checkpoints"
                                },
                                "checkpoint-counter": {
                                    "description": "",
                                    "type": "string",
                                    "default": "/checkpoint-counter"
                                },
                                "latch": {
                                    "description": "Defines the znode of the leader latch which is used to elect the leader",
                                    "type": "string",
                                    "default": "/leaderlatch"
                                },
                                "leader": {
                                    "description": "Defines the znode of the leader which contains the URL to the leader and the current leader session ID",
                                    "type": "string",
                                    "default": "/leader"
                                }
                            }
                        },
                        "client": {
                            "additionalProperties": false,
                            "type": "object",
                            "properties": {
                                "session_timeout": {
                                    "description": "Defines the session timeout for the ZooKeeper session in ms.",
                                    "type": "integer",
                                    "default": 60000
                                },
                                "connection_timeout": {
                                    "description": "Defines the connection timeout for ZooKeeper in ms.",
                                    "type": "integer",
                                    "default": 15000
                                },
                                "retry_wait": {
                                    "description": "Defines the pause between consecutive retries in ms.",
                                    "type": "integer",
                                    "default": 5000
                                },
                                "max_retry_attempts": {
                                    "description": "Defines the number of connection retries before the client gives up.",
                                    "type": "integer",
                                    "default": 3
                                }
                            }
                        },
                        "storageDir": {
                            "description": "Defines the directory in the state backend where the JobManager metadata will be stored (ZooKeeper only keeps pointers to it). Required for HA",
                            "type": "string",
                            "default": "/flink/recovery"
                        }
                    }
                },
                "job_delay": {
                    "description": "Defines the delay before persisted jobs are recovered in case of a recovery situation.",
                    "type": "string",
                    "default": "10 s"
                }
            }
        },
        "restart": {
            "additionalProperties": false,
            "type": "object",
            "properties": {
                "strategy": {
                    "description": "Default restart strategy to use in case that no restart strategy has been specified for the submitted job. Currently, it can be chosen from fixed delay restart strategy, failure rate restart strategy or no restart strategy. To use the fixed delay strategy you have to specify \"fixed-delay\". To use the failure rate strategy you have to specify \"failure-rate\". To turn the restart behaviour off you have to specify \"none\". Default value \"none\".",
                    "type": "string",
                    "default": "none"
                },
                "fixed_delay": {
                    "additionalProperties": false,
                    "type": "object",
                    "properties": {
                        "attempts": {
                            "description": "Number of restart attempts, used if the default restart strategy is set to fixed-delay",
                            "type": "integer",
                            "default": 1
                        },
                        "delay": {
                            "description": "Delay between restart attempts, used if the default restart strategy is set to fixed-delay",
                            "type": "string",
                            "default": "10 s"
                        }
                    }
                }
            }
        },
        "parallelism_default": {
            "description": "The default parallelism to use for programs that have no parallelism specified. (DEFAULT: 1). For setups that have no concurrent jobs running, setting this value to NumTaskManagers * NumSlotsPerTaskManager will cause the system to use all available execution resources for the program's execution. Note: The default parallelism can be overwriten for an entire job by calling setParallelism(int parallelism) on the ExecutionEnvironment or by passing -p <parallelism> to the Flink Command-line frontend. It can be overwritten for single transformations by calling setParallelism(int parallelism) on an operator",
            "type": "integer",
            "default": 1
        },
        "task_cancellation_interval": {
            "description": "Time interval between two successive task cancellation attempts in milliseconds ",
            "type": "integer",
            "default": 30000
        },
        "state_backend": {
            "additionalProperties": false,
            "type": "object",
            "properties": {
                "type": {
                    "description": "The backend that will be used to store operator state checkpoints if checkpointing is enabled. Supported backends: jobmanager and filesystem",
                    "type": "string",
                    "default": "filesystem"
                },
                "fsCheckpointsDir": {
                    "description": " Directory for storing checkpoints in a Flink supported filesystem. Note: State backend must be accessible from the JobManager, use file:// only for local setups",
                    "type": "string",
                    "default": "/flink/checkpoints"
                },
                "checkpointsDir": {
                    "description": " Directory for storing checkpoints in a Flink supported filesystem. Note: State backend must be accessible from the JobManager, use file:// only for local setups",
                    "type": "string",
                    "default": "/flink/state/checkpoints"
                },
                "savepointsDir": {
                    "description": " Directory for storing savepoints in a Flink supported filesystem. Note: State backend must be accessible from the JobManager, use file:// only for local setups",
                    "type": "string",
                    "default": "/flink/state/savepoints"
                }
            }
        }
    },
    "type": "object"
}
